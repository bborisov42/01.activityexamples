package com.innovator.activityexamples;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created on 5/6/2018.
 */
public class CustomDialog extends DialogFragment {

    public static final String DIALOG_PASSED_VALUE_EXTRA = "DIALOG_PASSED_VALUE_EXTRA";

    private String passedValue;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Activity activity = getActivity();

        LayoutInflater inflater = activity.getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        final TextView tv = dialogView.findViewById(R.id.dialog_input_et);
        tv.setText(passedValue);

        builder.setView(dialogView)
                .setTitle(getString(R.string.custom_dialog_example))
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (activity instanceof OnCustomDialogListener) {
                            ((OnCustomDialogListener)activity).onPositiveButtonClicked(tv.getText().toString());
                        }
                    }
                })
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (activity instanceof OnCustomDialogListener) {
                            ((OnCustomDialogListener)activity).onNegativeButtonClicked();
                        }
                        CustomDialog.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Bundle args = getArguments();
        if (args != null && args.containsKey(DIALOG_PASSED_VALUE_EXTRA)) {
            passedValue = args.getString(DIALOG_PASSED_VALUE_EXTRA);
        }
    }


    public interface OnCustomDialogListener {
        void onPositiveButtonClicked(String editTextValue);
        void onNegativeButtonClicked();
    }
}
