package com.innovator.activityexamples;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * TODO: (Lesson 1) (1)
     * Create SimpleDisplayTextActivity.
     * Use RelativeLayout.
     * Upon button click, open SimpleDisplayTextActivity which has an EditText, a TextView label and a Button.
     * Once the Button is clicked, the content of the EditText is displayed in the TextView.
     * Handle device rotation.
     */
    public void onSimpleDisplayTextActivityButtonClicked(View view) {
        CustomDialog customDialog = new CustomDialog();
        Bundle args = new Bundle();
        args.putString(CustomDialog.DIALOG_PASSED_VALUE_EXTRA, "Hello Custom Fragment");
        customDialog.setArguments(args);
        customDialog.show(getSupportFragmentManager(), "CustomDialog");
    }

    /**
     * TODO: (Lesson 1) (2)
     * Create PassDataActivity.
     * Add EditText in MainActivity.
     * Upon Button click, pass the content of the EditText to the started PassDataActivity
     * which will display it in a TextView
     */
    public void onPassDataActivityButtonClicked(View view) {

    }

    /**
     * TODO: (Lesson 1) (3)
     * Create ReturnDataActivity.
     * Upon Button click, start ReturnDataActivity.
     * Use ConstraintLayout.
     * In ReturnDataActivity, add an EditText and a Button.
     * Once the button is clicked, return the content of the EditText to MainActivity.
     * Handle result from ReturnDataActivity once back in MainActivity - show in Toast
     */
    public void onReturnDataButtonClicked(View view) {

    }

    /**
     * TODO: (Lesson 1) (4)
     * Create SharedPreferencesActivity which contains a TextView.
     * Use the EditText from (2). Get its value and store it in SharedPreferences.
     * When SharedPreferencesActivity opens, get the value from SharedPreferences and display it.
     */
    public void onSharedPreferencesButtonClicked(View view) {
    }


    /**
     * TODO: (Lesson 2) (5)
     * Parse the "books" JSON string.
     * Create a Book class with the necessary fields and methods.
     * Debug the code to verify the string is parsed correctly.
     */
    public void onParseBooksButtonClicked(View view) {

    }

    /**
     * TODO: (Lesson 2) (6)
     * Do it alone!
     * The same as (5) but for the "movies" JSON string.
     */
    public void onParseMoviesButtonClicked(View view) {

    }

    /**
     * TODO: (Lesson 2) (7)
     * Modify (2) to work with the CustomDialog.
     */
    private void openCustomDialog() {
        CustomDialog customDialog = new CustomDialog();
        Bundle args = new Bundle();
        args.putString(CustomDialog.DIALOG_PASSED_VALUE_EXTRA, "Hello from CustomDialog");
        customDialog.setArguments(args);
        customDialog.show(getSupportFragmentManager(), "CustomDialog");
    }
}
